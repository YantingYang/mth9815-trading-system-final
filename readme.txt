/**********************************************************/
Trading System Project
/**********************************************************/
Author: Yanting Yang

1. There are five lines:
a. marketdata.txt ->(By tcp-ip)-> BondMarketdata -> BondAlgoExecution -> BondExecution -> Bond TradeBooking -> print execution and out to a file as well

b. prices.txt ->(By tcp-ip)-> BondPricingdata -> BondAlgoSreaming -> Bond Sreaming -> print stream and out to a file as well

c. inquiry.txt ->(By tcp-ip)-> BondInquirydata -> BondInquiry

d. trades.txt ->(By tcp-ip)-> Trade data -> Bond Trade Booking -> Bond Position -> Bond Risk

e. All historical data: 
   for example, BondPositionHistoricalService is linked to BondPositionService by a listener(BondPositionService notifies historical service listener)
   it is similar to BondRiskHistoricalService, BondExecutionHistoricalService, BondStreamingHistoricalService and BondInquiryHistoricalService
   All historical services will out the data to a corresponding file.

2.I modified some original codes you sent me.
a. I added keyword virtual in all pure virtual functions
b. I added a function: GetBestBidOffer() in class OrderBook in file marketdataservice.hpp to get the best spread(to determine whether it is the tightest)
c. I added bond informations in file products.hpp at line 99 (I searched cusips online)
d. I added default constructors in all classes
e. I added a new class: tcp_ip, for basic use of tcp_ip
f. I added a function: AddPosition() in class PV01 in file riskservice.hpp


3. I created new classes: AlgoStream, AlgoExecution(the base class of BondAlgoExecution)
   In BondAlgoExecution, I use function execute to execute(first, we get the best spread from the orderbook, then compare to 1/120,
   I didn't use 1/128 to avoid floating number error. I randomly choose OrderType from three types as you mentioned in forum. And I
   oscilate between BUY and SELL.) 
   Then it notifies listeners of BondExecution and runs function ExecuteOrder() in class BondExecutionService, then data will be published.
   Onmessage method is empty in this class because it is replaced by ExecutueOrder().

   In Bond Trade Booking Service, I let onmessage function call BookTrade function because I think they are two different ways to book a trade,
   but they do the same things: insert trade information into map and notify listeners.
   
   In each connector class, I created a function PriceTransform() to transfer string like "99-010" to normal price number

   In BondHistoricalService, I put in 15 classes(5 services, 5 listeners and 5 connectors), 
       each Service class has a member of reference of connector to publish data




4. I used tcp-ip to read data from sockets. The server part is in project TCP-IP, it has a main.cpp file.
   DataGenerator file is in this project. All prices follow the instruction in the word(except inquiry.txt, the prices are random)
   I first created 4 data files, then transfer them from server to client.
   You can modify the file size by changing N and M in main.cpp, and you need to change the number N and M 
   correspondingly in another main.cpp in project Trading system.

5. When running the project, first run TCP-IP, wait for four txt files to generate(it will show 4 lines), then run Trading system.
   First, the server will print all market data and the client will print executions
   Second, the server will print all pricing data and the client will print the pricing data with Pricing Stream data
   Third, the server will print all trade data and the client will print the trade data as well
   Last, the server will print all inquiry data and the client will print the inquiry data as well

   After running the project, we will get all historical txt files.

6. In my handed version, M = N = 30, (i.e. 360 lines for market data and 180 lines for price data), it takes about 15-20 minutes to run.
   The length of the other two data files are fixed(60 for each cusip).

   I used command in my laptop: 
   g++ -m64 -g main.cpp -o server -lboost_system 
   ./server
and
   g++ -m64 -g main.cpp -o client -lboost_system
   ./client

   You may need to change the path of boost library


7. Output file explanation:
Streamings.txt:
Bond, BidPrice, BidHiddenQuantity, BidVisibleQuantity, OfferPrice, OfferHiddenQuantity, OfferVisibleQuantity

Positions.txt:
Bond, Position

Risks.txt:
Bond, Risk

Executions.txt:
ProductID, OrderID, SIDE, OrderType, IsChildOrder, Price, VisibleQuantity, HiddenQuantity

Inquiries.txt
Bond, InquiryID, price, SIDE, Quantity

GUI.txt
time, Ticker, MidPrice, Spread
