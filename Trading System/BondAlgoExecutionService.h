/**
Bond Algo Execution Service class
Created by Yanting Yang
*/

#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "soa.hpp"
#include "products.hpp"
#include "executionservice.hpp"
#include <unordered_map>


//AlgoExecution Base Class
template <class T>
class AlgoExecution {

private:
	ExecutionOrder<T> executionOrder;
	int cnt;  //Used to determine SELL or BUY

public:

	AlgoExecution() {
		//Default Ctor
		cnt = 0;
	}

	AlgoExecution(const T& _product, PricingSide _side, string _orderId, OrderType _orderType, double _price, double _visibleQuantity, double _hiddenQuantity, string _parentOrderId, bool _isChildOrder) {

		cnt = 0;
		executionOrder = ExecutionOrder<T>(_product, _side, _orderId, _orderType, _price,
			_visibleQuantity, _hiddenQuantity, _parentOrderId, false);

	}

	AlgoExecution(ExecutionOrder<Bond> _execution) {
		executionOrder = _execution;
	}

	ExecutionOrder<T> GetOrder() {
		return executionOrder;
	}

};



/**
Class BondAlgoExcution Service

*/

class BondAlgoExecutionService : public Service<string, AlgoExecution<Bond>> {



private:
	unordered_map<string, AlgoExecution<Bond>> Mydata;
	vector<ServiceListener<AlgoExecution<Bond>>*> MyListeners;
	long cnt;  //Used to determine SELL or BUY
	int number;

public:

	BondAlgoExecutionService() {
		number = 0;
		cnt = 1;
		Mydata = unordered_map<string, AlgoExecution<Bond>>();
	}

	virtual AlgoExecution<Bond>& GetData(string key) override {
		return Mydata[key];
	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(AlgoExecution<Bond>& data) {

		return;
	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<AlgoExecution<Bond>>* listener) {

		MyListeners.push_back(listener);
	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<AlgoExecution<Bond>>* >& GetListeners() const {

		return MyListeners;

	}

	void execute(OrderBook<Bond> _order) {

		auto BestBidOffer = _order.GetBestBidOffer();
		double Best_Bid = BestBidOffer.GetBidOrder().GetPrice();
		double Best_Offer = BestBidOffer.GetOfferOrder().GetPrice();

		
		double spread = Best_Offer - Best_Bid;

		double tol = 1.0/120;
		

		// Cross the spread when the spread is at its tightest
		if (spread <= tol) {

			number++;
			PricingSide side;
			if (cnt % 2) {
				side = BID;
			}
			else {
				side = OFFER;
			}

			string OrderID = to_string(cnt) + _order.GetProduct().GetProductId()
				+ to_string(_order.GetProduct().GetMaturityDate().year()) +
				_order.GetProduct().GetTicker() + to_string(number);

			OrderType type;
			int random = rand() % 3;
			if (random == 0) {
				type = LIMIT;
			}
			else if (random == 1) {
				type = MARKET;
			}
			else{
				type = IOC;
			}
			

			if (side == BID) {

				long Quantity = BestBidOffer.GetBidOrder().GetQuantity();
				long VisibleQuantity = Quantity * 1.0 / 3;
				long HiddenQuantity = Quantity * 1.0 / 3 * 2;

				auto Product = _order.GetProduct();

				//A new execution order
				AlgoExecution<Bond> algoexecution(Product, side, OrderID, type,
					Best_Bid, VisibleQuantity, HiddenQuantity, "Empty", false);
			
				//Insert into my dataMap
				Mydata.insert(std::make_pair(_order.GetProduct().GetProductId(), algoexecution));

				//Notify each listener
				for (auto listener : MyListeners)
					listener->ProcessAdd(algoexecution);



			}
			else if (side == OFFER) {

				long Quantity = BestBidOffer.GetOfferOrder().GetQuantity();
				long VisibleQuantity = Quantity * 1.0/ 3;
				long HiddenQuantity = Quantity * 1.0 / 3 * 2;

				//A new execution order
				AlgoExecution<Bond> algoexecution(_order.GetProduct(), side, OrderID, type,
					Best_Bid, VisibleQuantity, HiddenQuantity, "Empty", false);
				//Insert into my dataMap
				Mydata.insert(std::make_pair(_order.GetProduct().GetProductId(), algoexecution));

				//Notify each listener
				for (auto listener : MyListeners)
					listener->ProcessAdd(algoexecution);
			}
		}

	}







};




class BondAlgoExecutionListener : public ServiceListener<OrderBook<Bond>> {

	//From MarketDataService to Algo

private:
	// BondExecutionService object pointer
	BondAlgoExecutionService* service;


public:
	// ctor 
	BondAlgoExecutionListener(BondAlgoExecutionService* _service)
	{
		service = _service;
	}



public:
	// Override virtual function
	void ProcessAdd(OrderBook<Bond>& data) override {

		service->execute(data);
	}

	// Override virtual function
	void ProcessRemove(OrderBook<Bond>& data) override {

		//Empty
	}

	// Listener callback to process an update event to the Service
	void ProcessUpdate(OrderBook<Bond>& data) override
	{
		//Empty
	}




};







