#pragma once
#include <utility>
#include <boost/asio.hpp>
#include <ctime>
#include <cerrno>
#include <time.h>

using namespace boost::asio;
using ip::tcp;
using std::string;
using std::endl;


typedef boost::asio::buffers_iterator<boost::asio::streambuf::const_buffers_type> Myiterator;

std::pair<Myiterator, bool>
match_whitespace(Myiterator begin, Myiterator end)
{
	Myiterator i = begin;
	while (i != end) {
		if ((*i) == '\n' || (*i) == '\0')
			return std::make_pair(i, true);
		i++;
	}
	return std::make_pair(i, false);
}



void my_sleep(unsigned msec) {
	struct timespec req, rem;
	int err;
	req.tv_sec = msec / 1000;
	req.tv_nsec = (msec % 1000) * 1000000;
	while ((req.tv_sec != 0) || (req.tv_nsec != 0)) {
		if (nanosleep(&req, &rem) == 0)
			break;
		err = errno;
		// Interrupted; continue
		if (err == EINTR) {
			req.tv_sec = rem.tv_sec;
			req.tv_nsec = rem.tv_nsec;
		}
		// Unhandleable error (EFAULT (bad pointer), EINVAL (bad timeval in tv_nsec), or ENOSYS (function not supported))
		break;
	}
}
