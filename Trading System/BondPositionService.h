/**
Bond Position Service class
Created by Yanting Yang
*/

#pragma once
#include "positionservice.hpp"
#include "soa.hpp"
#include "products.hpp"
#include <unordered_map>


class BondPositionService :public PositionService<Bond> {

private:

	unordered_map<string, Position<Bond>> Mydata;
	vector<ServiceListener<Position<Bond>>*> MyListeners;


public:

	//Constructor
	BondPositionService() {

		Mydata = unordered_map<string, Position <Bond>>();

	}

	// Get data on our service given a key
	Position <Bond>& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Position <Bond>& data) override {
		

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Position <Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	const vector< ServiceListener<Position <Bond>>* >& GetListeners() const override {

		return MyListeners;

	}

	// Add a trade to the service
	void AddTrade(const Trade<Bond>& trade) override {

		//Get product id
		auto id = trade.GetProduct().GetProductId();

		//Get quantity
		auto quantity = trade.GetQuantity();
		auto side = trade.GetSide();
		if (side == SELL) {
			quantity = -quantity;
		}

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, Position<Bond>(trade.GetProduct())));
		}
		else {
			(iter->second).ChangePosition(id, quantity);
		}

		iter = Mydata.find(id);
		//Notify each Risk Service listener
		for (auto each : MyListeners) {
			each->ProcessAdd(iter->second);
		}

		
	}



};



class BondPositionListener : public ServiceListener<Trade<Bond>> {
	//Connect from trade to position

private:
	BondPositionService* service;


public:

	//Ctor
	BondPositionListener(BondPositionService* _service) {
		service = _service;
	}


	// Listener callback to process an add event to the Service
	void ProcessAdd(Trade<Bond>& data) override {
		service->AddTrade(data);
	}

	// Listener callback to process a remove event to the Service
	void ProcessRemove(Trade<Bond>& data) override{

		//Empty
	}

	// Listener callback to process an update event to the Service
	void ProcessUpdate(Trade<Bond>& data) override{

		//Empty

	}






};
