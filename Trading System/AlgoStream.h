/**
AlgoStream Class
Created by Yanting Yang
*/

#pragma once
#include "BondAlgoExecutionService.h"
#include "pricingservice.hpp"
#include "streamingservice.hpp"


class AlgoStream {

private:

	PriceStream<Bond>* priceStream;

public:

	AlgoStream(const Bond& _product, const PriceStreamOrder& _bidOrder, const PriceStreamOrder& _offerOrder)
	{
		priceStream = new PriceStream<Bond>(_product, _bidOrder, _offerOrder);
	}

	//Default ctor
	AlgoStream() {

	}

	PriceStream<Bond> GetStream() {
		return *priceStream;
	}

};