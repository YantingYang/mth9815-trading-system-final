/**
Bond Historical Data Service
Created by Yanting Yang
*/

#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <string>
#include "historicaldataservice.hpp"
#include "products.hpp"
#include "BondProduct.h"
#include "positionservice.hpp"
#include "executionservice.hpp"
#include "riskservice.hpp"
#include "inquiryservice.hpp"
#include <fstream>


/**
Bond Position Historical Data
*/

//Connector for Bond Position Historical Data
class BondPositionHistoricalDataConnector : public Connector<Position<Bond> > {

public:

	//ctor
	BondPositionHistoricalDataConnector() {

	}


	// Publish data to the Connector
	virtual void Publish(Position <Bond>& data) {

		auto bond = data.GetProduct();
		auto position = data.GetAggregatePosition();

		ofstream MyFile;
		MyFile.open("Positions.txt", ios::app);
		MyFile << bond << "," << position << '\n';


		return;

	}

	//flow data into the Service
	virtual void Subscribe(ifstream& _data) {

		//Empty
	}


};



class BondPositionHistoricalDataService : public HistoricalDataService<Position<Bond>> {
private:

	BondPositionHistoricalDataConnector* connector;
	unordered_map<string, Position<Bond>> Mydata;

public:
	// ctor
	BondPositionHistoricalDataService(BondPositionHistoricalDataConnector* _connector) {
		Mydata = unordered_map<string, Position<Bond>>();
		connector = _connector;
	}

	virtual Position<Bond>& GetData(string key) override{

		return Mydata[key];
	}

	//Work with connector
	virtual void OnMessage(Position<Bond>& data) override{
		//Empty
	
	}

	// Persist data to a store
	virtual void PersistData(string persistKey, const Position<Bond>& data){

		auto position = data;
		string id = position.GetProduct().GetProductId();

		long quantity = position.GetAggregatePosition();

		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata[id] = data;
		}

		auto TempData = data;
		connector->Publish(TempData);

		return;
	}


};


class BondPositionHistoricalDataListener : public ServiceListener<Position<Bond >> {

private:
	BondPositionHistoricalDataService* service;

public:
	// ctor
	BondPositionHistoricalDataListener(BondPositionHistoricalDataService* _service) {

		service = _service;
	}

	virtual void ProcessAdd(Position<Bond>& data) override {

		service->PersistData(data.GetProduct().GetProductId(), data);
		return;
	}

	virtual void ProcessRemove(Position<Bond>& data) override {
		//Empty
	}


	virtual void ProcessUpdate(Position<Bond>& data) override {
		//Empty
	}

};





/**
Bond Risk Historical Data Service
*/

//Connector for Bond Risk Historical Data
class BondRiskHistoricalDataConnector : public Connector<PV01<Bond>  > {

public:

	//ctor
	BondRiskHistoricalDataConnector() {

	}


	// Publish data to the Connector
	virtual void Publish(PV01 <Bond>& data) override {

		auto bond = data.GetProduct();
		auto risk = data.GetPV01();

		ofstream MyFile;
		MyFile.open("Risks.txt", ios::app);
		MyFile << bond << "," << risk << '\n';


		return;

	}

	//flow data into the Service
	virtual void Subscribe(ifstream& _data) {

		//Empty
	}


};




class BondRiskHistoricalDataService : public  HistoricalDataService<PV01<Bond>> {
private:
	unordered_map<string, PV01<Bond>> Mydata;
	BondRiskHistoricalDataConnector* connector;

public:
	// ctor
	BondRiskHistoricalDataService(BondRiskHistoricalDataConnector* _connector) {
		Mydata = unordered_map<string, PV01<Bond>>();
		connector = _connector;
	}

	virtual PV01<Bond>& GetData(string key){

		return Mydata[key];
	}

	//Work with connector
	virtual void OnMessage(PV01<Bond>& inquiry){

		//Empty
	}



	// Persist data to a store
	virtual void PersistData(string persistKey, const PV01<Bond> & data){

		auto id = data.GetProduct().GetProductId();
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata[id] = data;
		}

		auto TempData = data;
		connector->Publish(TempData);

		return;
	}

};



class BondRiskHistoricalDataListener : public ServiceListener<PV01<Bond >> {

private:
	BondRiskHistoricalDataService* service;

public:
	// ctor
	BondRiskHistoricalDataListener(BondRiskHistoricalDataService* _service) {

		service = _service;
	}

	virtual void ProcessAdd(PV01<Bond>& data) override {

		service->PersistData(data.GetProduct().GetProductId(), data);
		return;
	}

	virtual void ProcessRemove(PV01<Bond>& data) override {
		//Empty
	}


	virtual void ProcessUpdate(PV01<Bond>& data) override {
		//Empty
	}

};





/**
Bond Execution Historical Data Service
*/


//Connector for Bond Execution Historical Data
class BondExecutionHistoricalDataConnector : public Connector<ExecutionOrder<Bond>  > {

public:

	//ctor
	BondExecutionHistoricalDataConnector() {

	}


	// Publish data to the Connector
	virtual void Publish(ExecutionOrder<Bond>& data) override {

		auto bond = data.GetProduct();
		string OrderType;
		auto type = data.GetOrderType();
		if (type == FOK) {
			OrderType = "FOK";
		}
		else if (type == IOC) {
			OrderType = "IOC";
		}
		else if (type == MARKET) {
			OrderType = "MARKET";
		}
		else if (type == LIMIT) {
			OrderType = "LIMIT";
		}
		else {
			OrderType = "STOP";
		}


		ofstream MyFile;
		MyFile.open("Executions.txt", ios::app);
		MyFile << bond.GetProductId() << "OrderId: " << data.GetOrderId() << "    " << " PricingSide: " << (data.GetPricingSide() == BID ? "BID" : "ASK") << "      "
			<< "OrderType: " << OrderType << "    " << " IsChildOrder: " << (data.IsChildOrder() ? "True" : "False") << "\n"
			<< " Price: " << data.GetPrice() << "    " << " VisibleQuantity: " << data.GetVisibleQuantity()
			<< "    " << "HiddenQuantity: " << data.GetHiddenQuantity() << "\n\n";



		return;

	}

	//flow data into the Service
	virtual void Subscribe(ifstream& _data) {

		//Empty
	}


};



class BondExecutionHistoricalDataService : public  HistoricalDataService<ExecutionOrder<Bond>> {
private:
	unordered_map<string, ExecutionOrder<Bond>> Mydata;
	BondExecutionHistoricalDataConnector* connector;

public:
	// ctor
	BondExecutionHistoricalDataService(BondExecutionHistoricalDataConnector* _connector) {
		Mydata = unordered_map<string, ExecutionOrder<Bond>>();
		connector = _connector;
	}

	virtual ExecutionOrder<Bond>& GetData(string key) {

		return Mydata[key];
	}

	//Work with connector
	virtual void OnMessage(ExecutionOrder<Bond>& inquiry) {

		//Empty
	}



	// Persist data to a store
	void PersistData(string persistKey, const ExecutionOrder<Bond>& data){

		auto id = data.GetProduct().GetProductId();
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata[id] = data;
		}

		auto TempData = data;
		connector->Publish(TempData);

		return;
	}

};


class BondExecutionHistoricalDataListener : public ServiceListener<ExecutionOrder<Bond >> {

private:
	BondExecutionHistoricalDataService* service;

public:
	// ctor
	BondExecutionHistoricalDataListener(BondExecutionHistoricalDataService* _service) {

		service = _service;
	}

	virtual void ProcessAdd(ExecutionOrder<Bond>& data) override {

		service->PersistData(data.GetProduct().GetProductId(), data);
		return;
	}

	virtual void ProcessRemove(ExecutionOrder<Bond>& data) override {
		//Empty
	}


	virtual void ProcessUpdate(ExecutionOrder<Bond>& data) override {
		//Empty
	}

};





/**
Bond Inquiry Historical Data Service
*/

//Connector for Bond Inquiry Historical Data
class BondInquiryHistoricalDataConnector : public Connector<Inquiry<Bond>  > {

public:

	//ctor
	BondInquiryHistoricalDataConnector() {

	}


	// Publish data to the Connector
	virtual void Publish(Inquiry<Bond>& data) override {

		auto bond = data.GetProduct();
		auto inquiry_id = data.GetInquiryId();
		auto price = data.GetPrice();
		string side = (data.GetSide() == SELL) ? "SELL" : "BUY";
		auto quantity = data.GetQuantity();

		ofstream MyFile;
		MyFile.open("Inquirys.txt", ios::app);

		MyFile << bond << "," << inquiry_id << "," << price << "," << side << "," << quantity << '\n';

		return;

	}

	//flow data into the Service
	virtual void Subscribe(ifstream& _data) {

		//Empty
	}


};


class BondInquiryHistoricalDataService : public  HistoricalDataService<Inquiry<Bond>> {
private:
	unordered_map<string, Inquiry<Bond>> Mydata;
	BondInquiryHistoricalDataConnector* connector;

public:
	// ctor
	BondInquiryHistoricalDataService(BondInquiryHistoricalDataConnector* _connector) {
		Mydata = unordered_map<string, Inquiry<Bond>>();
		connector = _connector;
	}

	virtual Inquiry<Bond>& GetData(string key) {

		return Mydata[key];
	}

	//Work with connector
	virtual void OnMessage(Inquiry<Bond>& inquiry) {

		//Empty
	}



	// Persist data to a store
	void PersistData(string persistKey, const Inquiry<Bond>& data){

		auto id = data.GetProduct().GetProductId();
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata[id] = data;
		}

		auto TempData = data;
		connector->Publish(TempData);

		return;
	}

};


class BondInquiryHistoricalDataListener : public ServiceListener<Inquiry<Bond >> {

private:
	BondInquiryHistoricalDataService* service;

public:
	// ctor
	BondInquiryHistoricalDataListener(BondInquiryHistoricalDataService* _service) {

		service = _service;
	}

	virtual void ProcessAdd(Inquiry<Bond>& data) override {

		service->PersistData(data.GetProduct().GetProductId(), data);
		return;
	}

	virtual void ProcessRemove(Inquiry<Bond>& data) override {
		//Empty
	}


	virtual void ProcessUpdate(Inquiry<Bond>& data) override {
		//Empty
	}

};





/**
Bond Streaming Historical Service
*/

//Connector for Bond Streaming Historical Data
class BondStreamingHistoricalDataConnector : public Connector<PriceStream<Bond>> {

public:

	//ctor
	BondStreamingHistoricalDataConnector() {

	}


	// Publish data to the Connector
	virtual void Publish(PriceStream<Bond>& data) override {

		auto bond = data.GetProduct();
		auto bidOrder = data.GetBidOrder();
		auto askOrder = data.GetOfferOrder();

		ofstream MyFile;
		MyFile.open("Streamings.txt", ios::app);

		MyFile << bond << "," << bidOrder.GetPrice() << "," << bidOrder.GetHiddenQuantity() << ","
			<< bidOrder.GetVisibleQuantity() << "," << askOrder.GetPrice() << "," <<
			askOrder.GetHiddenQuantity() << "," << askOrder.GetVisibleQuantity() << '\n';

		return;

	}

	//flow data into the Service
	virtual void Subscribe(ifstream& _data) {

		//Empty
	}


};


class BondStreamingHistoricalDataService : public  HistoricalDataService<PriceStream<Bond>> {
private:
	unordered_map<string, PriceStream<Bond>> Mydata;
	BondStreamingHistoricalDataConnector* connector;

public:
	// ctor
	BondStreamingHistoricalDataService(BondStreamingHistoricalDataConnector* _connector) {
		Mydata = unordered_map<string, PriceStream<Bond>>();
		connector = _connector;
	}

	virtual PriceStream<Bond>& GetData(string key) {

		return Mydata[key];
	}

	//Work with connector
	virtual void OnMessage(PriceStream<Bond>& inquiry) {

		//Empty
	}



	// Persist data to a store
	void PersistData(string persistKey, const PriceStream<Bond>& data) {

		auto id = data.GetProduct().GetProductId();
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata[id] = data;
		}

		auto TempData = data;
		connector->Publish(TempData);

		return;
	}

};


class BondStreamingHistoricalDataListener : public ServiceListener<PriceStream<Bond >> {

private:
	BondStreamingHistoricalDataService* service;

public:
	// ctor
	BondStreamingHistoricalDataListener(BondStreamingHistoricalDataService* _service) {

		service = _service;
	}

	virtual void ProcessAdd(PriceStream<Bond>& data) override {

		service->PersistData(data.GetProduct().GetProductId(), data);
		return;
	}

	virtual void ProcessRemove(PriceStream<Bond>& data) override {
		//Empty
	}


	virtual void ProcessUpdate(PriceStream<Bond>& data) override {
		//Empty
	}

};
