/**
Bond Risk Service class
Created by Yanting Yang
*/


#pragma once
#include "riskservice.hpp"
#include "products.hpp"
#include <unordered_map>

class BondRiskService : public RiskService<Bond> {


private:

	unordered_map<string, PV01<Bond>> Mydata;
	vector<ServiceListener<PV01<Bond>>*> MyListeners;


public:

	//Ctor
	BondRiskService() {

		Mydata = unordered_map<string, PV01 <Bond>>();
	}

	// Add a position that the service will risk
	virtual void AddPosition(Position<Bond>& position){

		string id = position.GetProduct().GetProductId();

		long quantity = position.GetAggregatePosition();
		
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			PV01<Bond> pv01(position.GetProduct(), Risks[id], quantity);
			Mydata.insert(make_pair(id, pv01));
		}
		else {
			(iter->second).AddQuantity(quantity);
		}

		iter = Mydata.find(id);
		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(iter->second);
		}

	}

	// Get the bucketed risk for the bucket sector
	const PV01< BucketedSector<Bond> >& GetBucketedRisk(const BucketedSector<Bond>& sector) const {


		double res = 0;
		double total = 0;
		for (auto each : sector.GetProducts()) {
			//Get total sum
			res += Mydata.at(each.GetProductId()).GetPV01() * Mydata.at(each.GetProductId()).GetQuantity();
			total += Mydata.at(each.GetProductId()).GetQuantity();
		}
		return PV01<BucketedSector<Bond>>(sector, res * 1.0 / total, long(1));



	}


	// Get data on our service given a key
	virtual PV01 <Bond>& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(PV01 <Bond>& data) override {
		//Get product id
		auto id = data.GetProduct().GetProductId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(data);
		}

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<PV01 <Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<PV01 <Bond>>* >& GetListeners() const override {

		return MyListeners;

	}


};




class BondRiskServiceListener : public ServiceListener<Position<Bond>> {

	//From Position to Risk

private:
	BondRiskService* service;


public:

	//Ctor
	BondRiskServiceListener(BondRiskService* _service) {
		service = _service;
	}


	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(Position<Bond>& data) override {
		service->AddPosition(data);
	}

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(Position<Bond>& data) override {

		//Empty
	}

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(Position<Bond>& data) override {

		//Empty

	}





};