/**
Bond Streaming Service Class
Created by Yanting Yang
*/

#pragma once
#include "streamingservice.hpp"
#include "pricingservice.hpp"
#include <unordered_map>
#include "products.hpp"
#include <fstream>

class BondStreamingConnector : public Connector<PriceStream<Bond> > {

private:
	ofstream Myfile;

public:
	// ctor
	BondStreamingConnector() {
		Myfile.open("Bond Stream.txt", ios::trunc);

	}

	virtual void Subscribe() {

		//Empty
	}

	// print price stream 
	virtual void Publish(PriceStream<Bond>& data) override {

		auto BidOrder = data.GetBidOrder();
		auto bid = BidOrder.GetPrice();
		auto OfferOrder = data.GetOfferOrder();
		auto offer = OfferOrder.GetPrice();


		Myfile << data.GetProduct().GetTicker() << "," << bid << "," << offer << '\n';
		cout << data.GetProduct().GetTicker() << "," << bid << "," << offer << '\n';
	}
};



class BondStreamingService : public StreamingService<Bond> {

private:

	unordered_map<string, PriceStream<Bond>> Mydata;
	vector<ServiceListener<PriceStream<Bond>>*> MyListeners;
	BondStreamingConnector* MyConnector;

public:
	//Constructor
	BondStreamingService(BondStreamingConnector* connector) {

		Mydata = unordered_map<string, PriceStream<Bond>>();
		MyConnector = connector;

	}

	// Get data on our service given a key
	virtual PriceStream<Bond> & GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(PriceStream<Bond>& data) override {
		//Get product id
		auto id = data.GetProduct().GetProductId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(data);
		}

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<PriceStream<Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<PriceStream<Bond>>* >& GetListeners() const override {

		return MyListeners;

	}

	//Connect with Algo Stream
	void ConnectAlgo(AlgoStream& _data) {

		auto priceStream = _data.GetStream();
		OnMessage(priceStream);

	}

	virtual void PublishPrice(const PriceStream<Bond>& priceStream) override {

		PriceStream<Bond> stream = priceStream;
		MyConnector->Publish(stream);

	}

};





//From Algo Stream to Bond Stream
class BondStreamingListener : public ServiceListener<AlgoStream> {


private:
	// BondTradeBookingService object pointer
	BondStreamingService* service;


public:
	// ctor 
	BondStreamingListener(BondStreamingService* _service)
	{
		service = _service;
	}


	// Override virtual function
	void ProcessAdd(AlgoStream& data) override {

		//Add Algo Stream
		service->ConnectAlgo(data);
		//Publish Stream through Connector
		service->PublishPrice(data.GetStream());
	}

	// Override virtual function
	void ProcessRemove(AlgoStream& data) override {

		//Empty
	}

	// Listener callback to process an update event to the Service
	void ProcessUpdate(AlgoStream& data) override
	{
		//Empty
	}







};
