#pragma warning (disable: 4996)
#include "BondAlgoExecutionService.h"
#include "BondAlgoStreamingService.h"
#include "BondPositionService.h"
#include "BondPricingService.h"
#include "BondProduct.h"
#include "BondRiskService.h"
#include "BondTradeBookingService.h"
#include "BondHistoricalDataService.h"
#include "BondInquiryService.h"
#include "BondExecutionService.h"
#include "BondStreamingService.h"
#include "BondHistoricalDataService.h"
#include "BondMarketDataService.h"
#include "GUIService.h"
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <ctime>

using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;



using namespace std;

class connect_handler
{
public:
	connect_handler(io_service& ios)
		: io_service_(ios),
		timer_(ios),
		socket_(ios)
	{
		socket_.async_connect(
			tcp::endpoint(boost::asio::ip::address_v4::loopback(), 32123),
			boost::bind(&connect_handler::handle_connect, this,
				boost::asio::placeholders::error));

		timer_.expires_from_now(boost::posix_time::seconds(5));
		timer_.async_wait(boost::bind(&connect_handler::close, this));
	}

	void handle_connect(const boost::system::error_code& err)
	{
		if (err)
		{
			std::cout << "Connect error: " << err.message() << "\n";
		}
		else
		{
			std::cout << "Successful connection\n";
		}
	}

	void close()
	{
		socket_.close();
	}

private:
	io_service& io_service_;
	deadline_timer timer_;
	tcp::socket socket_;
};



int main() {

	//TCP-IP will generate 4 files, run it first

	int N = 30;   //Size for Market data, Change the number here if you want
	int M = 30; //Size for Market data, Change the number here if you want


	//Initialize all Bond Information
	vector<Bond> BondsInfo;
	string Ticker("T");
	for (int i = 0; i < 6; i++) {
		string id = Cusips[i];
		Bond NewBond(id, CUSIP, Ticker, Coupons[i], Maturities[i]);
		BondsInfo.push_back(NewBond);

	}

	//Initialize Bond Product for connectors
	BondProduct* bondProduct = new BondProduct();
	for (int i = 0; i < 6; i++) {
		bondProduct->OnMessage(BondsInfo[i]);
	}

	//Pricing
	BondPricingService* bondPricingService = new BondPricingService();
	BondPricingConnector* bondPricingConnector
		= new BondPricingConnector(bondPricingService, bondProduct, M);

	//Position
	BondPositionService* bondPositionService = new BondPositionService();
	BondPositionListener* bondPositionListener = new BondPositionListener(bondPositionService);

	//Market Data
	BondMarketDataService* bondMarketDataService = new BondMarketDataService();
	BondMarketDataConnector* bondMarketDataConnector = new BondMarketDataConnector(bondMarketDataService, bondProduct, N);
	
	
	//Algo and Execution
	BondAlgoExecutionService* bondAlgoExecutionService = new BondAlgoExecutionService();
	BondAlgoExecutionListener* bondAlgoExecutionListener = new BondAlgoExecutionListener(bondAlgoExecutionService);

	BondExecutionConnector* bondExecutionConnector = new BondExecutionConnector();
	BondExecutionService* bondExecutionService = new BondExecutionService(bondExecutionConnector);
	BondExecutionListener* bondExecutionListener = new BondExecutionListener(bondExecutionService);
	
	//From Market Data to Algo Execution
	bondMarketDataService->AddListener(bondAlgoExecutionListener);

	//From Algo to Execution
	bondAlgoExecutionService->AddListener(bondExecutionListener);

	//Trade Booking Service
	BondTradeBookingService* bondTradeBookingService = new BondTradeBookingService();
	BondTradeBookingConnector* bondTradeBookingConnector = new BondTradeBookingConnector(bondTradeBookingService, bondProduct);
	BondTradeBookingListener* bondTradeBookingListener = new BondTradeBookingListener(bondTradeBookingService);

	//From Execution to Trade Booking
	bondExecutionService->AddListener(bondTradeBookingListener);

	//From Trade Booking to Position
	bondTradeBookingService->AddListener(bondPositionListener);

	BondRiskService* bondRiskService = new BondRiskService();
	BondRiskServiceListener* bondRiskServiceListner = new BondRiskServiceListener(bondRiskService);

	//From Position to Risk
	bondPositionService->AddListener(bondRiskServiceListner);

	//Inquiry
	BondInquiryService* bondInquiryService = new BondInquiryService();
	BondInquiryConnector* bondInquiryConnector = new BondInquiryConnector(bondInquiryService, bondProduct);

	//Algo Stream Service
	BondAlgoStreamingService* bondAlgoStreamingService = new BondAlgoStreamingService();
	BondAlgoStreamingListener* bondAlgoStreamingListener = new BondAlgoStreamingListener(bondAlgoStreamingService);

	//From Price to Algo Streaming
	bondPricingService->AddListener(bondAlgoStreamingListener);

	BondStreamingConnector* bondStreamingConnector = new BondStreamingConnector();
	BondStreamingService* bondStreamingService = new BondStreamingService(bondStreamingConnector);
	BondStreamingListener* bondStreamingListener = new BondStreamingListener(bondStreamingService);

	//From Algo Stream to bond Stream
	bondAlgoStreamingService->AddListener(bondStreamingListener);

	//Bond Historical Data
	BondPositionHistoricalDataConnector* positionConnector = new BondPositionHistoricalDataConnector();
	BondPositionHistoricalDataService* historicalPositionService = new BondPositionHistoricalDataService(positionConnector);
	BondPositionHistoricalDataListener* historicalPositionListener = new BondPositionHistoricalDataListener(historicalPositionService);
	bondPositionService->AddListener(historicalPositionListener);

	BondRiskHistoricalDataConnector* riskConnector = new BondRiskHistoricalDataConnector();
	BondRiskHistoricalDataService* riskService = new BondRiskHistoricalDataService(riskConnector);
	BondRiskHistoricalDataListener* historicalRiskListener = new BondRiskHistoricalDataListener(riskService);
	bondRiskService->AddListener(historicalRiskListener);


	BondExecutionHistoricalDataConnector* executionConnector = new BondExecutionHistoricalDataConnector();
	BondExecutionHistoricalDataService* executionService = new BondExecutionHistoricalDataService(executionConnector);
	BondExecutionHistoricalDataListener* executionListener = new BondExecutionHistoricalDataListener(executionService);
	bondExecutionService->AddListener(executionListener);

	BondInquiryHistoricalDataConnector* inquiryConnector = new BondInquiryHistoricalDataConnector();
	BondInquiryHistoricalDataService* inquiryService = new BondInquiryHistoricalDataService(inquiryConnector);
	BondInquiryHistoricalDataListener* inquiryListener = new BondInquiryHistoricalDataListener(inquiryService);
	bondInquiryService->AddListener(inquiryListener);

	BondStreamingHistoricalDataConnector* streamConnector = new BondStreamingHistoricalDataConnector();
	BondStreamingHistoricalDataService* streamService = new BondStreamingHistoricalDataService(streamConnector);
	BondStreamingHistoricalDataListener* streamListener = new BondStreamingHistoricalDataListener(streamService);
	bondStreamingService->AddListener(streamListener);

	//GUI Service
	GUIConnector* guiConnector = new GUIConnector("GUI.txt");
	GUIService* guiService = new GUIService(guiConnector);
	GUIListener* guiListener = new GUIListener(guiService);
	bondPricingService->AddListener(guiListener);


	//Start the System

	//Connect with socket

	boost::asio::io_service io_service1;
	boost::asio::io_service io_service2;
	boost::asio::io_service io_service3;
	boost::asio::io_service io_service4;
	//socket creation
	tcp::socket socket_trade(io_service1);
	tcp::socket socket_price(io_service2);
	tcp::socket socket_marketdata(io_service3);
	tcp::socket socket_inquiry(io_service4);

	connect_handler ch1(io_service1);


	socket_marketdata.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 1379));
	socket_inquiry.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 8888));
	socket_price.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 2468));
	socket_trade.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 1357));
	

	
	bondMarketDataConnector->Subscribe(socket_marketdata);
	cout << "start\n";
	bondInquiryConnector->Subscribe(socket_inquiry);
	cout << "start\n";
	bondPricingConnector->Subscribe(socket_price);
	cout << "start\n";
	bondTradeBookingConnector->Subscribe(socket_trade);
	


	
	
	

	return 0;
}