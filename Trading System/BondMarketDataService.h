/**
Bond Market Data Service class
Created by Yanting Yang
*/


#pragma once
#include "marketdataservice.hpp"
#include "products.hpp"
#include <unordered_map>
#include "tcp_ip.h"
#include "BondProduct.h"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <iostream>
#include <boost/interprocess/file_mapping.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <cstddef>
#include <cstdlib>

//Input market data then
//Listener notify to BondAlgoExecutionService
//then notify to BondExecutionService (execute by a publish connecter, just cout)
//then notify to BondTradeBookingService
//then notify to BondPosition
using namespace boost::interprocess;


class BondMarketDataService : public MarketDataService<Bond> {

private:

	unordered_map<string, OrderBook <Bond>> Mydata;
	vector<ServiceListener<OrderBook <Bond>>*> MyListeners;


public:

	//Constructor
	BondMarketDataService() {

	}

	// Get data on our service given a key
	OrderBook <Bond>& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(OrderBook <Bond>& data) override {
		
		//Get product id
		auto id = data.GetProduct().GetProductId();
		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		//Notify each listener
		//From MarketData to BondAlgoExecution
		for (auto each : MyListeners) {
			each->ProcessAdd(data);
		}


	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<OrderBook<Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	const vector< ServiceListener<OrderBook <Bond>>* >& GetListeners() const override {

		return MyListeners;

	}

	// Get the best bid/offer order
	const BidOffer& GetBestBidOffer(const string& productId) override {

		auto orderbook = Mydata[productId];

		double BestBid = INT_MIN;
		Order BidOrd;
		for (auto bid : orderbook.GetBidStack()) {
			if (bid.GetPrice() > BestBid) {
				BestBid = bid.GetPrice();
				BidOrd = bid;
			}
		}

		double BestOffer = INT_MAX;
		Order OfferOrd;
		for (auto offer : orderbook.GetOfferStack()) {
			if (offer.GetPrice() < BestOffer) {
				BestOffer = offer.GetPrice();
				OfferOrd = offer;
			}
		}


		return BidOffer(BidOrd, OfferOrd);
			

	}

	// Aggregate the order book
	const OrderBook<Bond>& AggregateDepth(const string& productId) {

		auto orderbook = Mydata[productId];
		auto BidStack = orderbook.GetBidStack();
		auto OfferStack = orderbook.GetOfferStack();


		//Merge same prices together
		unordered_map<double, long> tempBid, tempOffer;
		for (auto each : BidStack) {
			if (tempBid.find(each.GetPrice()) != tempBid.end()) {
				tempBid[each.GetPrice()] += each.GetQuantity();
			}
			else {
				tempBid[each.GetPrice()] = each.GetQuantity();
			}
		}

		for (auto each : OfferStack) {
			if (tempOffer.find(each.GetPrice()) != tempOffer.end()) {
				tempOffer[each.GetPrice()] += each.GetQuantity();
			}
			else {
				tempOffer[each.GetPrice()] = each.GetQuantity();
			}
		}


		vector<Order> MyBid, MyOffer;
		for (auto each : tempBid) {
			MyBid.push_back(Order(each.first, each.second, BID));
		}

		for (auto each : tempOffer) {
			MyOffer.push_back(Order(each.first, each.second, OFFER));
		}

		return OrderBook<Bond>(orderbook.GetProduct(), MyBid, MyOffer);


	}

};



class BondMarketDataConnector : public Connector<OrderBook <Bond>> {


private:
	BondMarketDataService* service;
	BondProduct* product;
	int N; //Data file size

public:
	BondMarketDataConnector(BondMarketDataService* _service, BondProduct* _product, int _N) {

		service = _service;
		product = _product;
		N = _N;
	}

	//Transform string to price
	double PriceTransform(string& s) {

		int n = s.size();
		if (s[n - 1] == '+')
			s[n - 1] = '4';

		double res = 0;

		//Convert
		res += stoi(s.substr(0, n - 4));
		res += stoi(s.substr(n - 3, 2)) * 1.0 / 32;
		res += stoi(s.substr(n - 1, 1)) * 1.0 / 256;

		return res;

	}


	//flow data into the Service
	void Subscribe(tcp::socket& _socket) {

		string str;
		vector<Order> MyBid, MyOffer;


		// request/message from client
		const string msg = "Hello from Market!\n";
		boost::system::error_code error;
		boost::asio::write(_socket, boost::asio::buffer(msg), error);
		if (!error) {
			cout << "Market sent hello message!" << endl;
		}
		else {
			cout << "send failed: " << error.message() << endl;
		}

		int cnt = 0;
		
		// getting response from server
		while (1) {

			if (cnt >= 12 * N) {
				//Reach the end of file
				break;
			}


			boost::asio::streambuf receive_buffer;

			boost::asio::read_until(_socket, receive_buffer, match_whitespace);
			//my_sleep(10);
			string str1 = boost::asio::buffer_cast<const char*>(receive_buffer.data());

			while (str1.size() < 10) {
				boost::asio::streambuf receive_buffer;
				int size = boost::asio::read_until(_socket, receive_buffer, match_whitespace);
				string str1 = boost::asio::buffer_cast<const char*>(receive_buffer.data());
				//my_sleep(10);
			}

			stringstream templine(str1);

			string str;
			getline(templine, str);


			//cout << str << '\n';

			stringstream line(str);

			string temp;

			getline(line, temp, ',');
			Bond bond = product->GetData(temp);

			MyBid.clear();
			MyOffer.clear();


			for (int i = 0; i < 10; i++) {

				if (cnt >= 12 * N) {
					//Reach the end of file
					return;
				}
				cnt++;

				if (i > 0) {
					getline(line, temp, ',');
					bond = product->GetData(temp);
				}

				getline(line, temp, ',');

				auto price = PriceTransform(temp);

				getline(line, temp, ',');

				long quantity = stoi(temp);

				getline(line, temp, ',');
				PricingSide side;
				if (temp == "BID") {
					side = BID;
					MyBid.push_back(Order(price, quantity, BID));
				}
				else if (temp == "ASK") {
					side = OFFER;
					MyOffer.push_back(Order(price, quantity, OFFER));
				}
				else {
					cout << "wrong" <<  temp << '\n';
				}

				if (i != 9) {
					boost::asio::streambuf receive_buffer1;
					boost::asio::read_until(_socket, receive_buffer1, match_whitespace);
					str1 = boost::asio::buffer_cast<const char*>(receive_buffer1.data());
					//my_sleep(10);

					while (str1.size() < 10) {
						boost::asio::streambuf receive_buffer1;
						boost::asio::read_until(_socket, receive_buffer1, match_whitespace);
						str1 = boost::asio::buffer_cast<const char*>(receive_buffer1.data());
						
					}
					stringstream templine(str1);
					getline(templine, str);
					line = stringstream(str);
				}

			}

			if (MyBid.size() == 5 && MyOffer.size() == 5) {
				OrderBook<Bond> orderBook(bond, MyBid, MyOffer);
				service->OnMessage(orderBook);
			}


		}


		return;


	}



	void Publish(OrderBook <Bond>& data) {
		
		return;

	}


};

