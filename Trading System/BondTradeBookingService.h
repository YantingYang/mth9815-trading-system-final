/**
Bond Trade Booking Service Class
Created by Yanting Yang
*/


#pragma once
#include "tradebookingservice.hpp"
#include "soa.hpp"
#include "products.hpp"
#include <unordered_map>
#include "BondProduct.h"

class BondTradeBookingService : public TradeBookingService<Bond> {


private:

	unordered_map<string, Trade<Bond>> Mydata;
	vector<ServiceListener<Trade<Bond>>*> MyListeners;

public:


	//Constructor
	BondTradeBookingService() {
		Mydata = unordered_map<string, Trade<Bond>>();
	}

	// Get data on our service given a key
	virtual Trade <Bond>& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Trade <Bond>& data) override {
		BookTrade(data);

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Trade <Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Trade <Bond>>* >& GetListeners() const override {

		return MyListeners;

	}


	// Book the trade
	virtual void BookTrade(const Trade<Bond>& trade) override {

		//Get trade id
		auto id = trade.GetTradeId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, trade));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, trade));
		}

		//Notify each listener
		auto TempTrade = trade;
		for (auto each : MyListeners) {
			each->ProcessAdd(TempTrade);
		}

	}

};


class BondTradeBookingConnector :public Connector<Trade<Bond>> {

private:
	TradeBookingService<Bond>* service;
	BondProduct* product;


public:

	//ctor
	BondTradeBookingConnector(TradeBookingService<Bond>* _service, BondProduct* _product) {
		service = _service;
		product = _product;
	}


	// Publish data to the Connector
	virtual void Publish(Trade <Bond>& data) {

		return;

	}


	//Transform string to price
	double PriceTransform(string& s) {

		int n = s.size();
		if (s[n - 1] == '+')
			s[n - 1] = '4';

		double res = 0;

		//Convert
		res += stoi(s.substr(0, n - 4));
		res += stoi(s.substr(n - 3, 2)) * 1.0 / 32;
		res += stoi(s.substr(n - 1, 1)) * 1.0 / 256;

		return res;

	}



	//flow data into the Service
	//open trades.txt
	virtual void Subscribe(tcp::socket& _socket) {

		// request/message from client
		const string msg = "Hello from Trade!\n";
		boost::system::error_code error;
		boost::asio::write(_socket, boost::asio::buffer(msg), error);
		my_sleep(10);

		if (!error) {
			cout << "Trade sent hello message!" << endl;
		}
		else {
			cout << "send failed: " << error.message() << endl;
		}


		int cnt = 0;

		while (1) {
			

			boost::asio::streambuf receive_buffer;
			try {
				boost::asio::read_until(_socket, receive_buffer, "\n");
			}
			catch (const std::exception& e) {
				break;
			}
			string str = boost::asio::buffer_cast<const char*>(receive_buffer.data());
			//receive_buffer.consume(receive_buffer.size());

			cout << str;
			stringstream linee(str);

			string line1;
			getline(linee, line1, '\n');
			if (line1.size() < 30) {
				getline(linee, line1, '\n');
			}

			stringstream line(line1);


			string temp;
			getline(line, temp, ',');
			Bond bond = product->GetData(temp);
			getline(line, temp, ',');
			auto TradeID = temp;
			getline(line, temp, ',');
			long quantity = stoi(temp);
			getline(line, temp, ',');
			double price = stoi(temp);
			getline(line, temp, ',');
			auto book = temp;
			getline(line, temp, ',');
			Side side;
			if (temp == "BUY") {
				side = BUY;
			}
			else
			{
				side = SELL;
			}

			Trade<Bond> trade(bond, TradeID, price, book, quantity, side);

			service->OnMessage(trade);

			
			cnt++;
			if (cnt >= 60) {
				break;
			}

		}

		return;

	}


};


class BondTradeBookingListener : public ServiceListener<ExecutionOrder<Bond>> {

	//From Execution to TradeBooking

private:
	// BondTradeBookingService object pointer
	BondTradeBookingService* service;


public:
	// ctor 
	BondTradeBookingListener(BondTradeBookingService* _service)
	{
		service = _service;
	}



public:
	// Override virtual function
	void ProcessAdd(ExecutionOrder<Bond>& data) override {

		auto product = data.GetProduct();
		auto price = data.GetPrice();
		auto side = data.GetPricingSide();
		long quantity = data.GetVisibleQuantity();
		string order_id = data.GetOrderId();
		//Order id determines book and trade id
		string book, trade_id;
		if (order_id[0] == 'Y') {
			book = "TRSY1";
			trade_id = "YYT";
		}
		else if (order_id[0] == 'P') {
			book = "TRSY2";
			trade_id = "PPP";
		}
		else {
			book = "TRSY3";
			trade_id = "JAA";
		}


		Side SIDE;
		if (side == BID) {
			SIDE = SELL;
		}
		else {
			SIDE = BUY;
		}

		Trade<Bond> trade(product, trade_id, price, book, quantity, SIDE);
		service->BookTrade(trade);
	}

	// Override virtual function
	void ProcessRemove(ExecutionOrder<Bond>& data) override {

		//Empty
	}

	// Listener callback to process an update event to the Service
	void ProcessUpdate(ExecutionOrder<Bond>& data) override
	{
		//Empty
	}




};