/**
Bond Execution Service Class
Created by Yanting Yang
*/


#pragma once
#include "executionservice.hpp"
#include "products.hpp"
#include <unordered_map>
#include "BondAlgoExecutionService.h"


class BondExecutionConnector :public Connector<ExecutionOrder<Bond> > {
public:
	// ctor
	BondExecutionConnector() {

	}

	//prints them when it receives them
	virtual void Publish(ExecutionOrder<Bond>& data) override {

		auto bond = data.GetProduct();
		string OrderType;
		auto type = data.GetOrderType();
		if (type == FOK) {
			OrderType = "FOK";
		}
		else if (type == IOC) {
			OrderType = "IOC";
		}
		else if (type == MARKET) {
			OrderType = "MARKET";
		}
		else if (type == LIMIT) {
			OrderType = "LIMIT";
		}
		else {
			OrderType = "STOP";
		}


		//Print out
		cout << bond.GetProductId() <<"   " << "OrderId: " << data.GetOrderId() << "    " << " PricingSide: " << (data.GetPricingSide() == BID ? "BID" : "ASK") << "      "
			<< "OrderType: " << OrderType << "    " << " IsChildOrder: " << (data.IsChildOrder() ? "True" : "False") << "\n"
			<< " Price: " << data.GetPrice() << "    " << " VisibleQuantity: " << data.GetVisibleQuantity()
			<< "    " << "HiddenQuantity: " << data.GetHiddenQuantity() << "\n\n";



	}

	// publish only, no subsribe
	virtual void Subscribe() {
		//Empty
	}
};






class BondExecutionService : public ExecutionService<Bond> {

private:
	unordered_map<string, ExecutionOrder <Bond>> Mydata;
	vector<ServiceListener<ExecutionOrder <Bond>>*> MyListeners;
	BondExecutionConnector* MyConnector;   //Used to publish


public:
	BondExecutionService(BondExecutionConnector* connector){
		Mydata = unordered_map<string, ExecutionOrder <Bond>>();
		MyConnector = connector;

	}


	// Get data on our service given a key
	virtual ExecutionOrder <Bond>& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(ExecutionOrder <Bond>& data) override {
		
		//Empty
	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<ExecutionOrder <Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<ExecutionOrder <Bond>>* >& GetListeners() const override {

		return MyListeners;

	}


	virtual void ExecuteOrder(const ExecutionOrder<Bond>& order, Market market) override{

		auto data = order;
		//Get Order id
		auto id = data.GetOrderId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		MyConnector->Publish(data);

		//Notify each listener
		//from execution to bond trading book service
		for (auto each : MyListeners) {
			each->ProcessAdd(data);
		}

		
	}

	

};








class BondExecutionListener : public ServiceListener<AlgoExecution<Bond>> {

	//From AlgoExecution to Execution

private:
	BondExecutionService* service;

public:
	// ctor
	BondExecutionListener(BondExecutionService* _service) {
		service = _service;
	}


	virtual void ProcessAdd(AlgoExecution<Bond>& data) override {

		auto order = data.GetOrder();   //Type: ExecutionOrder<Bond>
		service->ExecuteOrder(order, BROKERTEC);

	}

	virtual void ProcessRemove(AlgoExecution<Bond>& data) override {

		//Empty
	}

	virtual void ProcessUpdate(AlgoExecution<Bond>& data) override {

		//Empty
	}






};








