/**
Bond Inquiry Service Class
Created by Yanting Yang
*/


#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <string>
#include "tcp_ip.h"
#include "inquiryservice.hpp"
#include "products.hpp"
#include "BondProduct.h"


class BondInquiryService : public InquiryService<Bond> {
private:
	unordered_map<string, Inquiry<Bond>> Mydata;
	vector<ServiceListener<Inquiry<Bond>>*> MyListeners;

public:
	// ctor
	BondInquiryService() {
		Mydata = unordered_map<string, Inquiry<Bond>>();
	}

	virtual Inquiry<Bond>& GetData(string key) override {

		return Mydata[key];
	}

	//Work with connector
	virtual void OnMessage(Inquiry<Bond>& inquiry) override {


		auto id = inquiry.GetInquiryId();
		Mydata.insert(make_pair(id, inquiry));
		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(inquiry);
		}
	}

	
	virtual void AddListener(ServiceListener<Inquiry<Bond> >* listener) override {

		MyListeners.push_back(listener);
	}


	virtual const vector<ServiceListener<Inquiry<Bond> >*>& GetListeners() const override {

		return MyListeners;
	}


	virtual void SendQuote(const string& inquiryId, double price) {
		//Empty
	}

	
	virtual void RejectInquiry(const string& inquiryId) {
		//Empty
	}


};


class BondInquiryConnector : public Connector<Inquiry<Bond>> {


private:
	BondInquiryService* service;
	BondProduct* product;

public:
	BondInquiryConnector(BondInquiryService* _service, BondProduct* _product) {

		service = _service;
		product = _product;
	}

	//Transform string to price
	double PriceTransform(string& s) {

		int n = s.size();
		if (s[n - 1] == '+')
			s[n - 1] = '4';

		double res = 0;

		//Convert
		res += stoi(s.substr(0, n - 4));
		res += stoi(s.substr(n - 3, 2)) * 1.0 / 32;
		res += stoi(s.substr(n - 1, 1)) * 1.0 / 256;

		return res;

	}


	//flow data into the Service
	void Subscribe(tcp::socket& _socket) {

		// request/message from client
		const string msg = "Hello from Inquiry!\n";
		boost::system::error_code error;
		boost::asio::write(_socket, boost::asio::buffer(msg), error);
		if (!error) {
			cout << "Inquiry sent hello message!" << endl;
		}
		else {
			cout << "send failed: " << error.message() << endl;
		}

		int cnt = 0;

		// getting response from server
		
		while (1) {
			boost::asio::streambuf receive_buffer;
			
			boost::asio::read_until(_socket, receive_buffer, "\n");
			//my_sleep(5);
			
			string str = boost::asio::buffer_cast<const char*>(receive_buffer.data());
			//cout << str.size() << '\n';
			stringstream linee(str);

			if (str.size() < 10) {
				continue;
			}

			string line1;
			getline(linee, line1, '\n');

			cout << line1 << '\n';
			stringstream line(line1);

			string temp;

			getline(line, temp, ',');
			string id = temp;

			getline(line, temp, ',');
			auto bond = product->GetData(temp);

			getline(line, temp, ',');
			Side side;
			if (temp == "BUY")
				side = BUY;
			else
				side = SELL;

			getline(line, temp, ',');
			long quantity = stol(temp);

			getline(line, temp, ',');
			double quote = PriceTransform(temp);

			getline(line, temp, ',');
			InquiryState state = RECEIVED;

			Inquiry<Bond> inquiry(id, bond, side, quantity, quote, state);
			service->OnMessage(inquiry);

			cnt++;
			if (cnt >= 60) {
				cout << "end\n";
				break;
			}
			
		}

		return;


	}



	virtual void Publish(Inquiry<Bond>& data) override {

		return;

	}


};
