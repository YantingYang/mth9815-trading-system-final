/**
Bond Algo Streaming Service class
Created by Yanting Yang
*/

#pragma once
#include "BondAlgoExecutionService.h"
#include "AlgoStream.h"
#include <unordered_map>
#include <vector>

using namespace std;


class BondAlgoStreamingService : public Service<string, AlgoStream> {

private:
	unordered_map<string, AlgoStream> Mydata;
	vector<ServiceListener<AlgoStream>*> MyListeners;
	int cnt;

public:

	//Ctor
	BondAlgoStreamingService() {
		cnt = 0;
		Mydata = unordered_map<string, AlgoStream>();
	}


	// Get data on our service given a key
	virtual AlgoStream& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(AlgoStream& data) override {
		//Get product id
		auto id = data.GetStream().GetProduct().GetProductId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		iter = Mydata.find(id);
		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(iter->second);
		}

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<AlgoStream>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<AlgoStream>* >& GetListeners() const override {

		return MyListeners;

	}



	void PublishPrice(Price<Bond>& _price) {

		string id = _price.GetProduct().GetProductId();
		double mid = _price.GetMid();
		double spread = _price.GetBidOfferSpread();
		double ask = mid + spread / 2;
		double bid = mid - spread / 2;

		cnt += 1;
		long VisibleSize = (cnt % 2 + 1) * 1000000;
		long HiddenSize = VisibleSize * 2;

		PriceStreamOrder BidOrder(bid, VisibleSize, HiddenSize, BID);
		PriceStreamOrder AskOrder(ask, VisibleSize, HiddenSize, OFFER);
		AlgoStream algoStream(_price.GetProduct(), BidOrder, AskOrder);

		OnMessage(algoStream);

		
	}

};


//From BondPricing Service to BondAlgoStreaming Service
class BondAlgoStreamingListener : public ServiceListener<Price<Bond> > {



private:
	BondAlgoStreamingService* service;

public:

	BondAlgoStreamingListener(BondAlgoStreamingService* _service) {

		service = _service;
	}

	virtual void ProcessAdd(Price<Bond>& price) override {
		service->PublishPrice(price);
	}

	
	virtual void ProcessRemove(Price<Bond>&) override {
		//Empty
	}

	
	virtual void ProcessUpdate(Price<Bond>&) override {
		//Empty
	}



};