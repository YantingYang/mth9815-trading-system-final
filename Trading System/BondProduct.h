/**
A class for all Bond Product Informations
Created by Yanting Yang
*/

#pragma once

#include "products.hpp"
#include <tuple>
#include <unordered_map>
#include <vector>
#include "pricingservice.hpp"

using namespace std;

class BondProduct : public Service<string, Bond> {

private:

	unordered_map<string, Bond> Mydata;   //Bond Map
	vector<ServiceListener<Bond>*> MyListeners;

public:
	//Constructor
	BondProduct() {

	}

	// Get data on our service given a key
	Bond& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Bond& data) override {
		//Get product id
		auto id = data.GetProductId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(data);
		}

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Bond>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	const vector< ServiceListener<Bond>* >& GetListeners() const override {

		return MyListeners;

	}





};

