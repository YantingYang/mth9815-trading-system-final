#pragma once
#include "BondPricingService.h"
#include "BondProduct.h"
#include <chrono>

using namespace std::chrono;

class GUIConnector : public Connector<Price<Bond> >
{
private:
	string MyFile;
public:
	GUIConnector(string file)
	{
		MyFile = file;
		ofstream out(MyFile,ios::trunc); //Rewrite each time
	}

	virtual void Publish(Price<Bond>& data) override
	{
		ofstream out(MyFile, ios::app);
		auto end = std::chrono::system_clock::now();
		std::time_t end_time = std::chrono::system_clock::to_time_t(end);

		stringstream time(std::ctime(&end_time));
		string temp;
		getline(time, temp, '\n');

		out << temp << "," << data.GetProduct().GetTicker()
			<< "," << data.GetMid() << "," << data.GetBidOfferSpread() << endl;
	}

	
};




class GUIService :public Service<string, Price<Bond> > {

private:
	long long int service_start_time;
	long long int last_quote_time;
	GUIConnector* connector;


public:
	GUIService(GUIConnector* _connector) 
	{
		connector = _connector;
		service_start_time = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		last_quote_time = service_start_time;
	}

	void PublishData(Price<Bond> data)
	{
		long long int current_epic = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		if (current_epic - last_quote_time > 300)
		{
			last_quote_time = current_epic;
			connector->Publish(data);
		}
	}

	virtual Price<Bond>& GetData(string key) override {

		Price<Bond> res;
		return res;
	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Price<Bond>& data) override {

		return;
	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Price<Bond>>* listener) override {

		return;
	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Price<Bond>>* >& GetListeners() const override{

		vector< ServiceListener<Price<Bond>>* > vec;
		return vec;
	}


};


class GUIListener : public ServiceListener<Price<Bond> >
{

private:
	GUIService* service;


public:
	GUIListener(GUIService* _service) {
		service = _service;

	}


	virtual void ProcessAdd(Price<Bond>& _data)
	{
		service->PublishData(_data);
	}


	virtual void ProcessRemove(Price<Bond>& _data) {
	
		//Empty
	}

	virtual void ProcessUpdate(Price<Bond>& _data) {
	
		//Empty
	}
};