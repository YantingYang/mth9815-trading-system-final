/**
Bond Pricing Service Class
Created by Yanting Yang
*/


#pragma once
#include "pricingservice.hpp"
#include "products.hpp"
#include <tuple>
#include <unordered_map>
#include <vector>
#include "BondProduct.h"
#include "tcp_ip.h"

using namespace std;

//Bond Pricing Service
class BondPricingService : public PricingService<Bond> {

private:

	unordered_map<string, Price <Bond>> Mydata;
	vector<ServiceListener<Price<Bond>>*> MyListeners;

public:
	//Constructor
	BondPricingService() {
		
		Mydata = unordered_map<string, Price <Bond>>();

	}

	// Get data on our service given a key
	virtual Price <Bond>& GetData(string key) override {

		return Mydata[key];

	}

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Price <Bond>& data) override {
		//Get product id
		auto id = data.GetProduct().GetProductId();

		//Insert data into Mydata
		auto iter = Mydata.find(id);
		if (iter == Mydata.end()) {
			Mydata.insert(make_pair(id, data));
		}
		else {
			Mydata.erase(iter);
			Mydata.insert(make_pair(id, data));
		}

		//Notify each listener
		for (auto each : MyListeners) {
			each->ProcessAdd(data);
		}

	}

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Price <Bond>>* listener) override {

		MyListeners.push_back(listener);

	}

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Price <Bond>>* >& GetListeners() const override {

		return MyListeners;

	}





};


//Bond Pricing Connector 
//Open prices.txt
class BondPricingConnector : public Connector<Price <Bond>> {

private:
	PricingService<Bond>* service;
	BondProduct* product;
	int N;

public:

	//ctor
	BondPricingConnector(PricingService<Bond>* _service, BondProduct* _product, int _N) {
		service = _service;
		product = _product;
		N = _N;
	}


	// Publish data to the Connector
	virtual void Publish(Price <Bond>& data) override {

		return;

	}

	//flow data into the Service
	virtual void Subscribe(tcp::socket& _socket) {

		// request/message from client
		const string msg = "Hello from Price!\n";
		boost::system::error_code error;
		boost::asio::write(_socket, boost::asio::buffer(msg), error);
		if (!error) {
			cout << "Price sent hello message!" << endl;
		}
		else {
			cout << "send failed: " << error.message() << endl;
		}


		// getting response from server
		int cnt = 0;
		

		while (1) {

			if (cnt >= 6 * N) {
				//Reach the end of file
				cout << "end\n";
				break;
			}
			

			boost::asio::streambuf receive_buffer;
			boost::asio::read_until(_socket, receive_buffer, match_whitespace);
			//my_sleep(20);
			string str = boost::asio::buffer_cast<const char*>(receive_buffer.data());
			stringstream linee(str);

			if (str.size() < 5) {
				//cnt++;
				continue;
			}

			string line1;
			getline(linee, line1, '\n');

			cout << line1 <<'\n'; 
			cnt++;


			stringstream line(line1);
			string temp;
			getline(line, temp, ',');
			string BondID = temp;
			auto bond = product->GetData(temp);
			getline(line, temp, ',');
			auto bid = PriceTransform(temp);   //Bid Price
			getline(line, temp, ',');
			//string tempp = temp.substr(0, temp.size() - 1);
			auto ask = PriceTransform(temp);   //Ask Price

			auto mid = (bid + ask) / 2;
			auto spread = ask - bid;

			Price<Bond> bondPrice(bond, mid, spread);

			service->OnMessage(bondPrice);

			
		}

	
		return;


	}

	//Transform string to price
	double PriceTransform(string& s) {

		int n = s.size();
		if (s[n - 1] == '+')
			s[n - 1] = '4';

		double res = 0;

		//Convert
		res += stoi(s.substr(0, n - 4));
		res += stoi(s.substr(n - 3, 2)) * 1.0 / 32;
		res += stoi(s.substr(n - 1, 1)) * 1.0 / 256;
		
		return res;

	}


};
