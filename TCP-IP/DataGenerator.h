#pragma once
#pragma warning (disable: 4996)
#include "BondInfo.h"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <iostream>


using namespace boost::interprocess;

void GeneratePrice(int n) {

	ofstream output;
	output.open("prices.txt", ios::trunc);
	//cusip - bid - ask

	//output << "cusip" << "," << "bid" << "," << "ask" << endl;

	for (int i = 0; i < 6; i++) {
		string cusip = Cusips[i];
		int last1 = 99;
		int last2 = 0;
		int last3 = 0;
		int lastspread = 128;  //0-002

		int direction = 1;  //Direction of Change

		output << cusip << "," << "99-000" << "," << "99-002" << '\n';

		for (int j = 0; j < n-1; j++) {

			last3 += direction;
			if (last3 > 7) {
				last3 -= 8;
				last2 += 1;
			}
			else if (last3 < 0) {
				last3 += 8;
				last2 -= 1;
			}

			if (last2 > 31) {
				last2 -= 32;
				last1 += 1;
			}
			else if (last2 < 0) {
				last2 += 32;
				last1 -= 1;
			}



			string part1 = to_string(last1);
			string part2, part3;
			if (last2 < 10)
				part2 = "0" + to_string(last2);
			else
				part2 = to_string(last2);
			if (last3 == 4)
				part3 = "+";
			else
				part3 = to_string(last3);

			string bid = part1 + "-" + part2 + part3;

			int temp1 = last1;
			int temp2 = last2;
			int temp3 = last3;

			string spread;
			int add = 1;
			if (lastspread == 64) {
				lastspread = 128;
				spread = "0-002";
				add = 2;
			}
			else {
				lastspread = 64;
				spread = "0-00+";
				add = 4;
			}


			if (last3 + add < 8) {
				last3 += add;
			}
			else {
				last3 = last3 + add - 8;
				last2 += 1;
			}

			if (last2 == 32) {
				last2 = 0;
				last1 = 100;
			}

			part1 = to_string(last1);
			if (last2 < 10)
				part2 = "0" + to_string(last2);
			else
				part2 = to_string(last2);
			if (last3 == 4)
				part3 = "+";
			else
				part3 = to_string(last3);



			string ask = part1 + "-" + part2 + part3;

			last1 = temp1;
			last2 = temp2;
			last3 = temp3;

			output << cusip << "," << bid << "," << ask << '\n';

			if (last1 == 100 && last2 == 31 && last3 > 2 && direction == 1) {
				direction = -1;
			}
			else if (last1 == 99 && last2 == 0 && last3 < 6 && direction == -1) {
				direction = 1;
			}
		}


	}

	cout << "Price Done!" << '\n';
	output.close();
}


void GenerateTrade() {

	ofstream output;
	output.open("trades.txt", ios::trunc);

	vector<string> books = { "TRSY1", "TRSY2", "TRSY3" };
	vector<string> sides = { "BUY","SELL" };
	vector<string> TradeID = { "YYT","PPP","JAA" };
	vector<long> Quantity = { 10000,20000,30000,40000,50000 };

	for (auto cusip : Cusips) {
		int cnt = 0;
		for (int i = 0; i < 10; i++) {

			string side = sides[cnt % 2];
			double price = (side == "BUY") ? 99.0 : 100.0;
			string quantity = to_string(Quantity[cnt % 5]);

			string prs = to_string(price);

			string res = cusip + "," + TradeID[cnt % 3] + "," + quantity + "," + prs.substr(0,6) + "," + books[cnt % 3] + "," + sides[cnt % 2];
			output << res << endl;
			cnt += 1;
		}
	}

	cout << "Trade Done!" << '\n';
	output.close();
}


void GenerateInquiry() {

	ofstream output;
	output.open("inquiry.txt", ios::trunc);

	int cnt = 0;
	vector<string> sides = { "BUY","SELL" };
	for (auto cusip : Cusips) {

		for (int i = 0; i < 10; i++) {

			int p1 = rand() % 2 + 99;
			int p2 = rand() % 32;
			int p3 = rand() % 8;

			string price, price2, price3;

			if (p3 == 4) {
				price3 = "+";
			}
			else {
				price3 = to_string(p3);
			}

			if (p2 < 10) {
				price2 = "0" + to_string(p2);
			}
			else {
				price2 = to_string(p2);
			}

			price = to_string(p1) + "-" + price2 + price3;

			string res = to_string(cnt) +
				"," + cusip + "," + sides[cnt % 2] +
				"," + to_string((rand() % 5 + 1) * 1000000) + "," +
				price + "," + "Received";

			output << res << endl;
			cnt += 1;
		}


	}


	cout << "Inquiry Done!" << '\n';
	output.close();
}


void GenerateMarketData(int n) {

	ofstream output;
	output.open("marketdata.txt", ios::trunc);

	vector<long> Quantity = { 1000000, 2000000,3000000,4000000,5000000 };

	int spread = 0;
	int IsUp = 1; //1 if goes up, 0 if goes down 

	for (auto cusip : Cusips) {

		//Mid Price starts from 99-000
		int last1 = 99;
		int last2 = 0;
		int last3 = 0;
		int lastspread = 128;  //0-002

		int direction = 1;  //Direction of change of MidPrice

		for (int i = 0; i < n; i++) {
			int bid1, bid2, bid3;
			int ask1, ask2, ask3;

			bid1 = last1;
			bid2 = last2;
			bid3 = last3;

			ask1 = last1;
			ask2 = last2;
			ask3 = last3;


			if (lastspread == 128) {
				spread = 1;
				IsUp = 1;
			}
			else if (lastspread == 64) {
				spread = 2;
			}
			else if (lastspread == 32) {
				spread = 3;
			}
			else {
				spread = 4;
				IsUp = 0;
			}

			if (IsUp) {
				lastspread /= 2;
			}
			else {
				lastspread *= 2;
			}

			ask3 += spread;
			if (ask3 > 7) {
				ask3 -= 8;
				ask2 += 1;
			}

			if (ask2 > 31) {
				ask2 -= 32;
				ask1 += 1;
			}


			string StrAsk2, StrAsk3;
			if (ask2 < 10) {
				StrAsk2 = "0" + to_string(ask2);
			}
			else {
				StrAsk2 = to_string(ask2);
			}

			if (ask3 == 4) {
				StrAsk3 = "+";
			}
			else {
				StrAsk3 = to_string(ask3);
			}

			string ask = to_string(ask1) + "-" + StrAsk2 + StrAsk3;

			bid3 -= spread;
			if (bid3 < 0) {
				bid3 += 8;
				bid2 -= 1;
			}

			if (bid2 < 0) {
				bid2 = 31;
				bid1 -= 1;
			}

			string StrBid2, StrBid3;
			if (bid2 < 10) {
				StrBid2 = "0" + to_string(bid2);
			}
			else {
				StrBid2 = to_string(bid2);
			}

			if (bid3 == 4) {
				StrBid3 = "+";
			}
			else {
				StrBid3 = to_string(bid3);
			}

			string bid = to_string(bid1) + "-" + StrBid2 + StrBid3;

			output << cusip << "," << bid << "," << Quantity[i % 5] << "," << "BID" << '\n';
			output << cusip << "," << ask << "," << Quantity[i % 5] << "," << "ASK" << '\n';


			last3 += direction;
			if (last3 > 7) {
				last3 -= 8;
				last2 += 1;
			}
			else if (last3 < 0) {
				last3 += 8;
				last2 -= 1;
			}

			if (last2 > 31) {
				last2 -= 32;
				last1 += 1;
			}
			else if (last2 < 0) {
				last2 += 32;
				last1 -= 1;
			}

			//When MidPrice touches the boundary
			if (last1 == 100 && last2 == 31 && last3 > 1 && direction == 1) {
				direction = -1;
			}
			else if (last1 == 99 && last2 == 0 && last3 <= 4 && direction == -1) {
				direction = 1;
			}

		}


	}


	cout << "Market Done!" << '\n';
	output.close();
}


