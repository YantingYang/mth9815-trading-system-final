//Server Part
//Created by Yanting Yang
#include <iostream>
#include <fstream>
#include <boost/asio.hpp>
#include "DataGenerator.h"
#include <ctime>
#include <cerrno>
#include <time.h>

void my_sleep(unsigned msec) {
	struct timespec req, rem;
	int err;
	req.tv_sec = msec / 1000;
	req.tv_nsec = (msec % 1000) * 1000000;
	while ((req.tv_sec != 0) || (req.tv_nsec != 0)) {
		if (nanosleep(&req, &rem) == 0)
			break;
		err = errno;
		// Interrupted; continue
		if (err == EINTR) {
			req.tv_sec = rem.tv_sec;
			req.tv_nsec = rem.tv_nsec;
		}
		// Unhandleable error (EFAULT (bad pointer), EINVAL (bad timeval in tv_nsec), or ENOSYS (function not supported))
		break;
	}
}


using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;
using namespace std;

string read_(tcp::socket& socket) {
	boost::asio::streambuf buf;
	boost::asio::read_until(socket, buf, "\n");
	string data = boost::asio::buffer_cast<const char*>(buf.data());
	return data;
}
void send_(tcp::socket& socket, const string& message) {
	const string msg = message;
	boost::asio::write(socket, boost::asio::buffer(msg));
	boost::asio::write(socket, boost::asio::buffer("\n"));
}



int main() {

	int m = 30;
	int n = 30;


	GeneratePrice(m);  //Change the number here
	GenerateTrade();
	GenerateInquiry();
	GenerateMarketData(n);  //Change the number here

	ifstream tradeFile("trades.txt");
	ifstream priceFile("prices.txt");
	ifstream inquiryFile("inquiry.txt");
	ifstream marketFile("marketdata.txt");


	boost::asio::io_service io_service1;
	boost::asio::io_service io_service2;
	boost::asio::io_service io_service3;
	boost::asio::io_service io_service4;
	

	//listen for new connection
	tcp::acceptor acceptor_trade(io_service1, tcp::endpoint(tcp::v4(), 1357));
	tcp::acceptor acceptor_price(io_service2, tcp::endpoint(tcp::v4(), 2468));
	tcp::acceptor acceptor_marketdata(io_service3, tcp::endpoint(tcp::v4(),1379));
	tcp::acceptor acceptor_inquiry(io_service4, tcp::endpoint(tcp::v4(), 8888));



	
	string str;

	tcp::socket socket_marketdata(io_service3);
	acceptor_marketdata.accept(socket_marketdata);

	//Marketdata.txt
	while (getline(marketFile, str)) {
		cout << str << '\n';
		send_(socket_marketdata, str);
		my_sleep(300);

	}
	cout << "Servent sent Market Message to Client!" << endl;

	tcp::socket socket_inquiry(io_service4);
	acceptor_inquiry.accept(socket_inquiry);

	//Inquiry.txt
	while (getline(inquiryFile, str)) {
		cout << str + '\n';
		send_(socket_inquiry, str);
		my_sleep(300);

	}
	cout << "Servent sent Inquiry Message to Client!" << endl;


	tcp::socket socket_price(io_service2);
	acceptor_price.accept(socket_price);
	//Price.txt
	while (getline(priceFile, str)) {
		cout << str << '\n';
		send_(socket_price, str);
		my_sleep(300);

	}
	cout << "Servent sent Price Message to Client!" << endl;


	//socket creation 
	tcp::socket socket_trade(io_service1);

	//waiting for connection
	acceptor_trade.accept(socket_trade);

	//write operation
	//trade.txt

	while (getline(tradeFile, str)) {
		send_(socket_trade, str);
		cout << str << '\n';
		my_sleep(300);
	}
	cout << "Servent sent Trade Message to Client!" << endl;

	


	
	


	return 0;
}