#pragma once
#include <vector>
#include <string>
#include "boost/date_time/gregorian/gregorian.hpp"
#include <unordered_map>

using namespace boost::gregorian;


using namespace std;
/***************************
Added Bond Information Below

****************************/


vector<string> Cusips = { "912828YT1", // 2Years
"912828TY6", // 3Years
"912828YV6", // 5Years
"912828YU8", // 7Years
"912828YS3", // 10Years
"912810SK5" }; // 30Years


vector<double> Coupons = { 0.0235, // 2Years
0.02876, // 3Years
0.02298, // 5Years
0.03001, // 7Years
0.03208, // 10Years
0.03434 }; // 30Years


vector<date> Maturities = { date(2022,Nov,30), // 2Years
date(2023,Nov,15), // 3Years
date(2024,Nov,30), // 5Years
date(2026,Nov,30), // 7Years
date(2030,Nov,15), // 10Years
date(2050,Nov,15) }; // 30Years


unordered_map<string, double> Risks = { {"912828YT1",0.01}, // 2Years
{"912828TY6",0.02}, // 3Years
{"912828YV6",0.04}, // 5Years
{"912828YU8",0.06}, // 7Years
{"912828YS3",0.09}, // 10Years
{"912810SK5",0.2} }; // 30Years


